import { Digit } from '../src/digit'
import { DigitData } from '../src/digitData'
import { expect } from 'chai';
import 'mocha';

describe('DigitData : getNthLineDisplay function', () => {

  it('should return the representation of the seond line of LED digit 8 when getNthLineDisplay(1) is called on an instance of Digit 8', () => {
    const symbolHuit: string[][] = [[' ', '_', ' '], ['|', '_', '|'], ['|', '_', '|']];
    const eightInstance: Digit = new Digit(symbolHuit);
    const expected: string =
      `|_| `;
    expect(eightInstance.getNthLineDisplay(1)).to.eql(expected);
  });

});