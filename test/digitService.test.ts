import { DigitService } from '../src/digitService'
import { expect } from 'chai';
import 'mocha';

const digitService = new DigitService();

describe('DigitService : isNumber function', () => {

  it('should return true when input is 123', () => {
    const input: string = '123'
    const result: boolean = digitService.isNumber(input);
    expect(result).to.be.true;
  });

  it('should return false when input is 255f.3', () => {
    const input: string = '255f.3'
    const result: boolean = digitService.isNumber(input);
    expect(result).to.be.false;
  });

});

describe('DigitService : getSplittedDigit function', () => {

    it('should return array [1] when input is 1', () => {
      const input: number = 1
      const result = digitService.getSplittedDigit(input);
      expect(result).to.eql([1]);
    });

    it('should return array [7, 3, 1] when input is 731', () => {
      const input = 731
      const result = digitService.getSplittedDigit(input);
      expect(result).to.eql([7, 3, 1]);
    });

  });

  describe('DigitService : displayNthFinalLine function', () => {

    it('should return the representation of the first line of LED digit 1 when input is array [1]', () => {
      const input: number[] = [1];
      const expected: string =
      `    \n`;
      const result = digitService.displayNthFinalLine(input, 0);
      expect(result).to.eql(expected);
    });

    it('should return the representation of the third line of LED number 123 when input is array [1, 2, 3]', () => {
        const input: number[] = [1,2,3]
        const expected: string =
        `  | |_   _| \n`
        const result = digitService.displayNthFinalLine(input, 2);
        expect(result).to.eql(expected);
      });

  });

  describe('DigitService : convert function', () => {

    it('should return the representation of LED digit 7 when input is string "7"', () => {
      const input: string = '7';
      const expected: string =
      ` _  \n` +
      `  | \n` +
      `  | \n`;
      const result = digitService.convert(input);
      expect(result).to.eql(expected);
    });

    it('should return the representation of LED number 1234567890 when input is string "1234567890"', () => {
        const input: string = '1234567890';
        const expected: string =
        `     _   _       _   _   _   _   _   _  \n` +
        `  |  _|  _| |_| |_  |_    | |_| |_| | | \n` +
        `  | |_   _|   |  _| |_|   | |_|   | |_| \n`;
        const result = digitService.convert(input);
        expect(result).to.eql(expected);
      });

      it('should ignore leading zero', () => {
        const input: string = '012';
        const expected: string =
        `     _  \n` +
        `  |  _| \n` +
        `  | |_  \n`;
        const result = digitService.convert(input);
        expect(result).to.eql(expected);
      });

  });