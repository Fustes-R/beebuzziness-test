import { DigitData } from '../src/digitData'
import { Digit } from '../src/digit'
import { expect } from 'chai';
import 'mocha';

describe('DigitData : getDigitObject function', () => {

  it('should be an instance of Digit representing 0 when calling getDigitObject(0)', () => {
    const symbolZero: string[][] = [[' ', '_', ' '], ['|', ' ', '|'], ['|', '_', '|']];
    const testInstanceProperties: string[][] = new Digit(symbolZero).symbols;
    expect(DigitData.getDigitObject(0)).to.be.an('object');
    expect(DigitData.getDigitObject(0).symbols).to.eql(testInstanceProperties);
  });

});