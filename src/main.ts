import * as readline from 'readline';
import { DigitService } from './digitService';

const rl: readline.Interface = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});
const digitService: DigitService = new DigitService();

rl.setPrompt('Entrer un nombre à afficher> ');
rl.prompt();

rl.on('line', (line): void => {
    try {
        console.log(digitService.convert(line.trim()));
    } catch (e) {
        console.log(e.message);
    }
    rl.prompt();
}).on('close', function () {
    console.log('Bonne journée!');
    process.exit(0);
});