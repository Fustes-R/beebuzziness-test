import { Digit } from './digit'

const symbolZero: string[][] = [[' ', '_', ' '], ['|', ' ', '|'], ['|', '_', '|']];
const symbolUn: string[][] = [[' ', ' ', ' '], [' ', ' ', '|'], [' ', ' ', '|']];
const symbolDeux: string[][] = [[' ', '_', ' '], [' ', '_', '|'], ['|', '_', ' ']];
const symbolTrois: string[][] = [[' ', '_', ' '], [' ', '_', '|'], [' ', '_', '|']];
const symbolQuatre: string[][] = [[' ', ' ', ' '], ['|', '_', '|'], [' ', ' ', '|']];
const symbolCinq: string[][] = [[' ', '_', ' '], ['|', '_', ' '], [' ', '_', '|']];
const symbolSix: string[][] = [[' ', '_', ' '], ['|', '_', ' '], ['|', '_', '|']];
const symbolSept: string[][] = [[' ', '_', ' '], [' ', ' ', '|'], [' ', ' ', '|']];
const symbolHuit: string[][] = [[' ', '_', ' '], ['|', '_', '|'], ['|', '_', '|']];
const symbolNeuf: string[][] = [[' ', '_', ' '], ['|', '_', '|'], [' ', ' ', '|']];

interface DigitArray {
    [key: number]: Digit
}

 export class DigitData {

    private static digitObjectArray: DigitArray = {
        0: new Digit(symbolZero),
        1: new Digit(symbolUn),
        2: new Digit(symbolDeux),
        3: new Digit(symbolTrois),
        4: new Digit(symbolQuatre),
        5: new Digit(symbolCinq),
        6: new Digit(symbolSix),
        7: new Digit(symbolSept),
        8: new Digit(symbolHuit),
        9: new Digit(symbolNeuf),
    }

    public static getDigitObject(digit: number): Digit {
        return this.digitObjectArray[digit];
    }

}