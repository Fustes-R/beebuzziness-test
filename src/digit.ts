export class Digit {

    private _symbols: string[][];

    constructor(number: string[][]) {
        this._symbols = number;
    }

    get symbols(): string[][] {
        return this._symbols;
    }

    set symbols(symbols: string[][]) {
        this._symbols = symbols;
    }

    public getNthLineDisplay(nthLine: number): string {
        let result: string = '';
        for(let singleSymbol of this._symbols[nthLine]) {
            result+= singleSymbol;
        }
        result+= ' ';
        return result;
    }

}