import { DigitData } from './digitData'

export class DigitService {

    public convert(line: string): string {

        if(!this.isNumber(line)){
            throw new Error('Entrée invalide !')
        }

        let number: number = parseInt(line);
        const splittedDigit: number[] = this.getSplittedDigit(number);
        let finalResult: string = '';
        finalResult += this.displayNthFinalLine(splittedDigit, 0);
        finalResult += this.displayNthFinalLine(splittedDigit, 1);
        finalResult += this.displayNthFinalLine(splittedDigit, 2);

        return finalResult;
    }

    public displayNthFinalLine(splittedNumber: number[], nthLine: number): string {
        let result: string = '';
        for(let elem of splittedNumber) {
            result += DigitData.getDigitObject(elem).getNthLineDisplay(nthLine);
        }
        result += '\n'
        return result;
    }

    public getSplittedDigit(number: number): number[] {
        let digits: number[] = [];
        const sNumber: string = number.toString();
        for (var i = 0, len = sNumber.length; i < len; i += 1) {
            digits.push(+sNumber.charAt(i));
        }
        return digits;
    }

    public isNumber(val: string): boolean {
        return /^\d+$/.test(val);
    }
}
